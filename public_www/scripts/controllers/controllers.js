
var app = angular.module("Controllers", ["Factories"]);

//Dirty implementation of controller for users.
app.controller("AddUserController", function ($scope, UserFactory) {
	console.log("AddUserController loaded");
	$scope.accounttype = 1;
	$scope.startTime = "09:00";
	$scope.endTime = "17:00";
	$scope.addUserButton = function () {
		console.log("Add new user function is being called");
		var newuser = new UserFactory();
		newuser.id = $scope.id;
		newuser.firstname = $scope.firstname;
		newuser.lastname = $scope.lastname;
		newuser.workphone = $scope.workphonenumber;
		newuser.homephone = $scope.homephonenumber;
		newuser.worktimetotal = $scope.worktimetotal;
		newuser.accounttype = $scope.accounttype;
		newuser.flex = $scope.flex;
		newuser.workStartTime = $scope.startTime;
		newuser.workEndTime = $scope.endTime;
		//newuser.datestartedworking = $scope.datestartedworking;
		newuser.email = $scope.email;
		newuser.password = $scope.password; // this is not actual password implementation
		newuser.$save(); //save the user.
		// reset the scope $variables
		$scope.id = "";
		$scope.firstname = "";
		$scope.lastname = "";
		$scope.workphonenumber = "";
		$scope.homephonenumber = "";
		$scope.worktimetotal = "";
		$scope.flex = "";
		$scope.datestartedworking = "";
		$scope.email = "";
		$scope.password = "";
		$scope.accounttype = "1";

	}
	$('.sclockpicker').clockpicker()
	.find('input').change(function () {
		$scope.startTime = this.value;
	});

	$('.eclockpicker').clockpicker()
	.find('input').change(function () {
		$scope.endTime = this.value;
	});
});

app.controller("MyProfileController", function ($scope, UserFactory, TokenFactory, CheckPwordFactory) {
	console.log("MyProfileController Loaded");

	//finds logged user to myprofile-page
	console.log("Search for user/employee");
	var userID = TokenFactory.getId();
	console.log('userID:' + userID);
	UserFactory.get({
		id: userID
	}, function (user) {
		console.log('user:');
		console.log(user);
		$scope.userdetails = user;
	});

	//editing my profile
	$scope.updatemyprofile = function (userid) {
		console.log('id to update', userid);
		var updateuser = $scope.userdetails;
		//checking if new password is given
		var dirty = $scope.myProfileForm.newpw.$dirty;
		var dbpw = updateuser.password;
		var oldpw = $scope.oldpw;
		var newpw = $scope.newpw;

		if (dirty == true){
			
			console.log("dbpw:"+ dbpw);
			console.log("dirty:"+ dirty);
			console.log("oldpw:"+ oldpw);
			console.log("newpw:"+ newpw);
			
			//if given old password is same as old password in db
			CheckPwordFactory.check(userID, oldpw).then(function (data) {
				console.log(data.data.message);
				
				//Old password matches with new one
				if (data.data.message == "PasswordMatch") {
					
					updateuser.password = newpw;
					$scope.oldpw = "";
					$scope.newpw = "";
					$scope.confirmpw = "";
					$scope.warning = "";
					
					UserFactory.update({
					"id": userid
					}, updateuser, function () {
						console.log("update");
					});
					
				}//else giving warning about wrong password
				else {
					$scope.warning = 'Wrong password!';
				}
			});
		}
		
		//If dirty = not true, we are only updating phone numbers
		else {
			console.log("Updating phone numbers only.");
			UserFactory.update({
					"id": userid
					}, updateuser, function () {
						console.log("update");
					});
		}
		
	}
});

//checking if new passwords match
app.directive('pwCheck', [function () {
    return {
      	require: 'ngModel',
      	link: function (scope, elem, attrs, ctrl) {
	        var firstPassword = '#' + attrs.pwCheck;
	        $(elem).add(firstPassword).on('keyup', function () {
	          	scope.$apply(function () {
	            	var v = elem.val()===$(firstPassword).val();
	            	ctrl.$setValidity('pwmatch', v);
	          	});
	        });
      	}
    }
}]); 
  

app.controller("ListandSearchUserController", function ($scope, UserFactory) {
	console.log("ListandSearchUserController loaded");
	var list = UserFactory.query(function () {
			$scope.userlist = list;
		});
	//initialise variables for filtering users
	$scope.query = {};
	$scope.queryBy = '$';
	//Search one employee
	$scope.search = function () {
		//$scope.show = true;
		console.log("Search for user/employee");
		UserFactory.get({
			id: $scope.id
		}, function (user) {
			console.log(user);
			$scope.userdetails = user;
		});
	}
	//Deleting one employee
	$scope.delete  = function (userid) {
		console.log("userid" + userid);
		var confirm = window.confirm("Are you Sure?");
		if (confirm == true) {
			for (var i in list) {
				if (list[i].id == userid) {
					console.log("match found");
					UserFactory.delete ({
						"id": userid
					}, function () {});
					list.splice(i, 1);
				}
			}
		}
	}
	//Editing employee detail
	$scope.update = function (userid) {
		console.log('id to update', userid);
		UserFactory.update({
			"id": userid
		}, $scope.userdetails, function () {
			console.log("update");

		});
	}
	//functionality for the edit button in listandsearchuser doesn't load all of the information yet :)
	$scope.openEditBox = function (userid) {
		console.log("open up editbox for user id: " + userid);
		$scope.editform = true;
		UserFactory.get({
			id: userid
		}, function (user) {
			console.log(user);
			$scope.userdetails = user;
		});
	};
	$('.sclockpicker').clockpicker()
	.find('input').change(function () {
		$scope.startTime = this.value;
	});

	$('.eclockpicker').clockpicker()
	.find('input').change(function () {
		$scope.endTime = this.value;
	});
});

//Vacations Controller
app.controller("VacationsController", function ($scope, HourFactory, TokenFactory) {
	//console.log("VacationsController");
	var annualvacation = 0;
	var sickLeave = 0;
	var compensatedLeaves = 0;
	HourFactory.query({
			id: TokenFactory.getId()
		}, function (user) {

			console.log(user); //currently calling for particular id

			for (var i = 0; i < user.length; i++) {

				if (user[i].itemtype == 2) {
					sickLeave = sickLeave + 1;
				} else if (user[i].itemtype == 3) {
					annualvacation = annualvacation + 1;
				} else if (user[i].itemtype == 4) {
					compensatedLeaves = compensatedLeaves + 1;
				}
				//console.log(annualvacation,sickLeave,compensatedLeaves);
				$scope.annualvacation = annualvacation;
				$scope.sickLeave = sickLeave;
				$scope.compensatedLeaves = compensatedLeaves;
				//console.log(user[i].worktimeday);

			}
		});
	/* //this is useful later don't delete
	$scope.vacationdata = function () {
		//Making sure variablees are reset
		annualvacation = 0;
		sickLeave = 0;
		compensatedLeaves = 0;
		//--
		console.log("getting vacation data");
		console.log($scope.id);
		HourFactory.query({
			id: TokenFactory.getId()
		}, function (user) {

			console.log(user); //currently calling for particular id

			for (var i = 0; i < user.length; i++) {

				if (user[i].itemtype == 2) {
					sickLeave = sickLeave + 1;
				} else if (user[i].itemtype == 3) {
					annualvacation = annualvacation + 1;
				} else if (user[i].itemtype == 4) {
					compensatedLeaves = compensatedLeaves + 1;
				}
				//console.log(annualvacation,sickLeave,compensatedLeaves);
				$scope.annualvacation = annualvacation;
				$scope.sickLeave = sickLeave;
				$scope.compensatedLeaves = compensatedLeaves;
				//console.log(user[i].worktimeday);

			}
		});
	};*/
});

app.controller("LoginController", function ($scope, LoginFactory, TokenFactory, $location) {

	//Functionality for button login() in login.html
	$scope.login = function () {
		//Disables login button so you can't initiate multiple logins accidentally
		$scope.buttonDisabled = true;
		console.log("Password from controller: " + $scope.pword);
		//Send email+password to loginfactory which will forward them to server for checking
		LoginFactory.login($scope.email, $scope.pword).then(function (data) {
			$scope.email = "";
			$scope.pword = "";
			/*
			If server returns NoUser, user doesn't exist in mongo.
			If server returns WrongPassword, the password didn't match the one in mongo.
			If server returns TokenGranted, login successful.
			*/
			if (data.data.message == "NoUser") {
				console.log("No user!");
				$scope.checked = true;
				$scope.buttonDisabled = false;
				TokenFactory.unsetAll();
			} else if (data.data.message == "WrongPassword") {
				console.log("Wrong password!");
				$scope.checked = true;
				$scope.buttonDisabled = false;
				TokenFactory.unsetAll();
			} else if (data.data.message == "TokenGranted") {
				console.log("Object id from database is: " + data.data.objectid);
				TokenFactory.setToken(data.data.token, data.data.userid, data.data.objectid, data.data.accounttype);
				//Received success from login
				console.log("ID from factory is: " + TokenFactory.getId());
				console.log("Object ID from factory is: " + TokenFactory.getObjectId());
				//re-direct to main page (temporarily manage users) because login was successful
				$location.path('/calendar');
			}
		});
	};

});

//Simple logout controller. It simply clears localstorage and re-directs back to login page.
app.controller("logoutController", function ($scope, $location, WeekFactory) {

	$scope.logout = function () {
		
		//Use the method below to query week data for specific id and year
		//WeekFactory.query({id:500,week:43,year:2017});
		
		localStorage.clear();
		$location.path('/login');
	}
});

//Controller for navigation bar visibility.
app.controller("NavigationbarController", function ($scope, TokenFactory) {
	$scope.isLogged = function () {
		if (TokenFactory.getToken()) {
			return true;
		} else {
			return false;
		}
	}
	$scope.isAdmin = function () {
		if (TokenFactory.getAccounttype() == 2) {
			return true;
		} else {
			return false;
		}
	}
	

});
