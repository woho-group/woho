var app = angular.module("Calendarctrl", ["Factories"]);
//var g_starttime = 0;
//var g_endtime = 0;
var g_selectedDate;
var worktimeday;
var g_assignHourDay = 0;
var g_flexHour = 0;
var g_hourCalc = 0;
var g_minuteCalc = 0;
var g_flag = true;
var g_SDateIsAllDay = false;
app.controller('CalendarController', function ($scope, HourFactory, TokenFactory, UserFactory) {

	var userID = TokenFactory.getId();
	console.log(userID);
	UserFactory.get({
		id: userID
	}, function (user) // Getting work time assign and flex hour curently for Id 1
	{
		//console.log(user.worktimetotal);
		//console.log("flex in userfactory", user.flex);
		//console.log("userstart time", user.workStartTime);
		g_assignHourDay = user.worktimetotal;
		g_flexHour = user.flex; //assigning flex hour to global variable
		$scope.flex = user.flex; //scope in html page
		$scope.startTime = user.workStartTime;
		$scope.endTime = user.workEndTime;
	});
	var personalCalendarUrl = '/api/hoursdaily/' + userID;

	//This part sets up calendarEventType dropdownbox and is used in saving the event.
	$scope.calendarEventType = {
		availableOptions: [{
				id: '1',
				name: 'Hours Done'
			}, {
				id: '2',
				name: 'Sick Leave'
			}, {
				id: '3',
				name: 'Vacation'
			}, {
				id: '4',
				name: 'Compensated Leave'
			}
		],
		selectedOption: {
			id: '1',
			name: 'Hours Done'
		} //This sets the default value of the select in the ui
	};

	$('#calendar').fullCalendar({
		timeFormat: 'HH:mm',
		ignoreTimezone: false,
		eventSources: [
			// your event source
			{
				type: 'GET',
				url: personalCalendarUrl, // use the `url` property
				headers: {
					'token': TokenFactory.getToken()
				},
				//color: 'blue',    // an option!
				//textColor: 'black',  // an option!
				timezone: 'local'
			}
		],
		disableResizing: true,
		allDaySlot: true,
		displayEventTime: true,
		displayEventEnd: true,
		left: 'title',
		axisFormat: 'HH:mm',
		slotDuration: '00:60:00',
		fixedWeekCount: false,
		firstDay: 1, //sets the week to start from monday
		header: {
			left: 'prev,next today',
			center: 'title',
			right: 'month,agendaWeek,agendaDay'
		},
		defaultView: 'month',
		g_selectedDate: g_selectedDate,

		dayClick: function (date, jsEvent, view) {

			//assigns the dayClicked to g_selectedDate
			g_selectedDate = date.format();
			
			//splits the date format YYYY-MM-DDTHH:MM:SS selected in case it has more exact information
			//so we are left with the YYYY-MM-DD
			//this is only done this way due to the way we want clockpickers to work and have a default times.
		
			g_selectedDate = g_selectedDate.split("T", 1)[0];
			//$(this).css('background-color', add8e6);
			/*
			console.log();
			if (jsEvent.allDay){
				g_SDateIsAllDay = true;
			} else {
				g_SDateIsAllDay = false;
			}*/
		},
		eventClick: function (calEvent, jsEvent, view) {

			//$('#calendar').fullCalendar('removeEvents', calEvent.id);
		},
		eventRender: function (event, element) {
			element.append("<span id='closeon' class='glyphicon glyphicon-trash'></span>");
			element.find("#closeon").click(function () {
				console.log("Event ID : " + event._id);
				var eid = event._id;
				swal({
					title: "Are you sure you want to delete this?",
					text: "Deletion is permanent!",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Yes, delete it!",
					cancelButtonText: "Cancel",
					closeOnConfirm: true,
					closeOnCancel: true
					},
					function(isConfirm){
						if(isConfirm){
					deleteCalendarEventFromDatabase(eid);
					$('#calendar').fullCalendar('removeEvents', event._id);
						}
					}
				);
			});
		},

		selectable: true,
		selecthelper: true,
		weekNumbers: true
	});

	//ClockPickers
	$('.sclockpicker').clockpicker()
	.find('input').change(function () {
		// TODO: time changed
		//console.log("sclockvalue" + this.value);
		$scope.startTime = this.value;
		//g_starttime = $scope.startTime;
	});

	$('.eclockpicker').clockpicker()
	.find('input').change(function () {
		// TODO: time changed
		//console.log("eclock" + this.value);
		$scope.endTime = this.value;
		//g_endtime = $scope.endTime;
	});

	function checkTimeAndSelectedDate() {
		if (($scope.endTime <= $scope.startTime) || (!g_selectedDate)) {
			swal("Please check your start and end times and date");
			return false;
		} else if ($scope.startTime === undefined  || $scope.endTime === undefined ) {
			swal("Please select valid start and end times");
			return false;
		}
		else {
			return true;
		}
	}
	//calculating hours done in a day
	function hourscalculation() {
		l_startTimeString = $scope.startTime.split(":");
		//console.log(l_startTimeString, typeof l_startTimeString);
		l_endTimeString = $scope.endTime.split(":");
		//console.log(l_endTimeString);
		//Getting work done in a day by converting time in date format
		var startDate = new Date(0, 0, 0, l_startTimeString[0], l_startTimeString[1], 0);
		var endDate = new Date(0, 0, 0, l_endTimeString[0], l_endTimeString[1], 0);
		var diff = endDate.getTime() - startDate.getTime();
		var hours = Math.floor(diff / 1000 / 60 / 60);
		diff -= hours * 1000 * 60 * 60;
		var minutes = Math.floor(diff / 1000 / 60);
		//Checking if day selected is saturday or sunday if so automatically adds 07:30 to worktimeday.
		var l_selectedDay = moment(g_selectedDate).format('dddd'); //Getting day out of date
		if (l_selectedDay == 'Saturday' || l_selectedDay == 'Sunday') {
      console.log(g_assignHourDay, typeof g_assignHourDay);
     /* g_assignHourDay = g_assignHourDay.replace(':','.');
      console.log(g_assignHourDay, typeof g_assignHourDay);
      var l_assignHour = g_assignHourDay.slice(0,2);
      l_assignHour = Number(l_assignHour);
      var l_assignMinute = g_assignHourDay.slice(-2);
      l_assignMinute = Number(l_assignMinute);
      console.log(l_assignHour, l_assignMinute, typeof l_assignHour)*/
			hours = hours + 7;
			minutes = minutes + 30;
      console.log(hours,minutes);
			if (minutes >= 60) { //if minute is more then 60 then add 1 to hour and adjust minute value
				hours = hours + 1;
				minutes = minutes - 60;
			}
		}
		worktimeday = (hours < 9 ? "0" : "") + hours + ":" + (minutes < 9 ? "0" : "") + minutes;
		console.log(worktimeday, typeof worktimeday);
		return worktimeday;
	}

	//calculating flex hours
	function flexhour() {

		//console.log(worktimeday);
		//console.log(g_flexHour, typeof g_flexHour);
		g_flexHour = g_flexHour.replace(":", ".");
		//console.log(g_flexHour);
		//if flex hour is in negative then slice the string from 1-3(Leaving neagtive sign)
		//Calculating flex hour and minute
		var l_flexSign = Number(g_flexHour);
		//console.log(l_flexSign);
		if (Math.sign(l_flexSign) == -1) {
			var l_flexHourhour = g_flexHour.slice(1, 3);
			//console.log(l_flexHourhour);
			l_flexHourhour = -l_flexHourhour;
		} else {
			var l_flexHourhour = g_flexHour.slice(0, 2); //console.log(l_flexHourhour, typeof l_flexHourhour);
			l_flexHourhour = Number(l_flexHourhour);
			//console.log(l_flexHourhour);
		}
		//Getting minutes of flex hour
		var l_flexHourminute = g_flexHour.slice(-2);
		//Converting hour and minute to number
		l_flexHourhour = Number(l_flexHourhour);
		//console.log(l_flexHourhour);
		l_flexHourminute = Number(l_flexHourminute);
		//console.log(l_flexHourminute);

		//Getting hours assign and done in date format
		l_assignHourString = g_assignHourDay.split(":");
		worktimeday = worktimeday.split(":");
		var l_assignHourDate = new Date(0, 0, 0, l_assignHourString[0], l_assignHourString[1], 0);
		var l_hoursDoneDate = new Date(0, 0, 0, worktimeday[0], worktimeday[1], 0);

		//Checking if assign hour is greater then hours done in day then subtract
		//hours from flex
		if (l_assignHourDate > l_hoursDoneDate) {
			//calculating flex hour in a day
			var diff = l_assignHourDate.getTime() - l_hoursDoneDate.getTime();
			var hours = Math.floor(diff / 1000 / 60 / 60);
			//console.log(hours);

			diff -= hours * 1000 * 60 * 60;
			var minutes = Math.floor(diff / 1000 / 60);
			//console.log(minutes);
			//getting flex hours and minutes to subtract using date format
			var l_flextoSubtract = (hours < 9 ? "0" : "") + hours + ":" + (minutes < 9 ? "0" : "") + minutes;
			//console.log(l_flextoSubtract);
			l_flextoSubtract = l_flextoSubtract.replace(":", ".");
			var l_flextoSubtractHour = Number(l_flextoSubtract.slice(0, 2)); //console.log(l_flextoSubtractHour);
			var l_flextoSubtractminute = Number(l_flextoSubtract.slice(-2)); //console.log(l_flextoSubtract);
			//calculating updated flex hour and minute
			var l_updatedflexHour = l_flexHourhour - l_flextoSubtractHour;
			var l_updatedflexminute = Math.abs(l_flexHourminute - l_flextoSubtractminute);
			//console.log(l_updatedflexHour, l_updatedflexminute);

			//Code below is just to display  the information properly.
			//if hour value is less then 10 but graeter then 0 add 0 to value to make it in correct format
			if ((l_updatedflexHour < 10) && (l_updatedflexHour >= 0)) {
				l_updatedflexHour = String("0" + Math.abs(l_updatedflexHour));
			} else if (l_updatedflexHour < 0) {
				g_flexHour = String(("-" + "0" + Math.abs(l_updatedflexHour) + ":" + l_updatedflexminute));
			} else {
				l_updatedflexHour = String(l_updatedflexHour);
			}
			//if minute is single value then add 0 after it
			if ((l_updatedflexminute / 10 == 0) || (l_updatedflexminute < 10)) {
				l_updatedflexminute = String(l_updatedflexminute + "0")
			} else {
				l_updatedflexminute = String(l_updatedflexminute);
			}
			// if flex hour is less then 0 then add - sign to it just for indicating that you have negative flex

			g_flexHour = String(l_updatedflexHour + ":" + l_updatedflexminute);
			//console.log(g_flexHour);
		}

		//if work done is more then the assign hours then add remaining to flex.
		else {
			var diff = l_hoursDoneDate.getTime() - l_assignHourDate.getTime();
			var hours = Math.floor(diff / 1000 / 60 / 60);
			//console.log(hours, typeof hours);
			diff -= hours * 1000 * 60 * 60;
			var minutes = Math.floor(diff / 1000 / 60);
			var l_flextoAdd = (hours < 9 ? "0" : "") + hours + ":" + (minutes < 9 ? "0" : "") + minutes;
			//console.log(l_flextoAdd);
			l_flextoAdd = l_flextoAdd.replace(":", "."); //converting flex to add in hour minute
			var l_flextoAddHour = Number(l_flextoAdd.slice(0, 2));
			//console.log(l_flextoAddHour);
			var l_flextoAddminute = Number(l_flextoAdd.slice(-2));
			//console.log(l_flextoAddminute);
			//calculating flex hour and minutes to updated flex
			var l_updatedflexHour = (l_flextoAddHour + l_flexHourhour);
			var l_updatedflexminute = (l_flextoAddminute + l_flexHourminute);
			//if total minute is greater then 60 then adjusting it to hour value
			if (l_updatedflexminute >= 60) {
				l_updatedflexHour = l_updatedflexHour + 1;
				l_updatedflexminute = l_updatedflexminute - 60;
				//console.log(l_updatedflexHour, l_updatedflexminute, typeof l_updatedflexminute);
			}
			//Formatiing hour minute and converting them to string for display
		//if hour value is less then 10 but graeter then 0 adding 0 before it
			if ((l_updatedflexHour < 10) && (l_updatedflexHour >= 0)) {
				l_updatedflexHour = String("0" + l_updatedflexHour);
			} else if (l_updatedflexHour < 0) {
				//console.log("<0")
				g_flexHour = String("-0" + Math.abs(l_updatedflexHour) + ":" + l_updatedflexminute);
			} else
				l_updatedflexHour = String(l_updatedflexHour);
			//checking minute is divisible by 10 (10,20,30...) so extra 0 in string after number
			if ((l_updatedflexminute / 10 == 0) && (l_updatedflexminute != 0)) {
				l_updatedflexminute = String(l_updatedflexminute + "0")
			}
			//checking minute if it is less then 10 add 0 before the value
			else if (l_updatedflexminute < 10) {
				l_updatedflexminute = String("0" + l_updatedflexminute);
			} else
				l_updatedflexminute = String(l_updatedflexminute);
			//console.log(l_updatedflexHour, l_updatedflexminute, typeof l_updatedflexminute);
			//Displaying on page with format HH:MM

			g_flexHour = (l_updatedflexHour + ":" + l_updatedflexminute);

		}
		return g_flexHour;
	}

	//---------------------///
	//--Time Conversions --///
	//---------------------///
	function convertTimesToDateFormat(dateSelected, timeToConvertonvert) {
		//function to convert stime and etime to date type
		//converts dateSelected and timeToConvertonvert into a moment object
		//and saves it to convertedDate
		var convertedDate = moment(dateSelected + "T" + timeToConvertonvert + "Z").tz("Europe/Helsinki").format();
		//the T separates date and minutes Z the timezone
		return convertedDate;
	}
	function convertTimesToCalendarFormat(dateSelected, timeToConvertonvert) {
		//converts date to the calendarformat so that timezones are displayed correctly
		var convertedCalendarDate = moment(dateSelected + "T" + timeToConvertonvert).format();
		return convertedCalendarDate;
	}

	//---------------------//
	//calendar manipulation//
	//---------------------//
	function addEventToCalendar(eventStart, eventEnd, eventTitle, eventAllDay) {
		var newEvent = {
			title: eventTitle,
			start: eventStart,
			end: eventEnd,
			allDay: eventAllDay
		};
		if (checkCalendarEventOverlap(newEvent) == false) {
			console.log(newEvent);
			$('#calendar').fullCalendar('renderEvent', newEvent, false);
			return true;
		} else {
			return false;
		};
	}

	function checkCalendarEventOverlap(event) {
		/*
		Function checks if the event overlaps with another event already in the calendar
		TODO: extra checks for item types 2,3,4 since they don't have a start time Maybe use allday?
		 */
		var start = new Date(event.start);
		var end = new Date(event.end);

		var overlap = $('#calendar').fullCalendar('clientEvents', function (ev) {
				if (ev == event)
					return false;
				var estart = new Date(ev.start);
				var eend = new Date(ev.end);

				return (Math.round(estart) / 1000 < Math.round(end) / 1000 && Math.round(eend) > Math.round(start));
			});
		var hasAllDayEvent = $('#calendar').fullCalendar('clientEvents', function (ev){
			console.log(ev);
			console.log("the event: ");
			console.log(event);
		});
		if (overlap.length) {
			console.log("overlap");
			//either move this event to available timeslot or remove it
			return true;
		} else {
			return false;
		}
	}

	function deleteCalendarEventFromDatabase(calendarEvent_id) {
		console.log("calendarEvent_id" + calendarEvent_id);
		console.log("Userid: " + userID);
		HourFactory.delete ({"id": calendarEvent_id	});
		
	}

	function getWeekNumber() {
		/*
		Making sure correct week number is added to db. Because of localisation issues,
		different standards for week numbering etc. we need to check the data and modify if needed.
		Eg. Sunday is considered first day of the week in moment by default, but that won't work for us.
		We could use isoWeek standard, but the calendar week numbering isn't using that so we would have
		data that differs from our calendar.
		 */
		console.log("The date is " + g_selectedDate + " so moment gives us week number " + moment(g_selectedDate).week()
			 + " with a day of " + moment(g_selectedDate).day());
		var weekNumber;
		//If the day is sunday
		if (moment(g_selectedDate).day() == 0) {
			//If it's sunday and week number is 1, that's where most conflicts occur
			if (moment(g_selectedDate).week() == 1) {
				//Sunday, week 1, so to get correct week number we query the previous day
				weekNumber = moment(moment(g_selectedDate).day(-1)).week();
			} else {
				//Otherwise it's just week -1 to get the correct week for this sunday
				weekNumber = moment(g_selectedDate).week() - 1;
			}
		} else {
			//If it's not sunday, we can just use week number moment gives us
			weekNumber = moment(g_selectedDate).week();
		}
		console.log("After modifications, our week number is: " + weekNumber);
		return weekNumber;
	}

	//The save event to database and Calendar function.
	$scope.save = function () {

		g_flag = checkTimeAndSelectedDate(); //check that the timers are not some weird value
		
		if (g_flag == true) {
			//if the times are correct and proceed to calendar posting
			//console.log("gflag value: " + g_flag);
			var isHoursDone;
			var calendarSTime;
			var calendarETime;
			if ($scope.calendarEventType.selectedOption.id == 1) {
				isHoursDone = false; //allDay Event to false allows time range posting eg. 09:00 - 17:00
				calendarSTime = convertTimesToCalendarFormat(g_selectedDate, $scope.startTime)
				calendarETime = convertTimesToCalendarFormat(g_selectedDate, $scope.endTime)
			} else {
				isHoursDone = true; //this sets allDay type of the event to true
				calendarSTime = moment(g_selectedDate).minute(1).format();
				calendarETime = moment(g_selectedDate).hour(23).minute(59).second(59).millisecond(999).format();
				console.log("caletime"+calendarETime);
			}
			//console.log(calendarSTime, calendarETime, isHoursDone);
			
			//Check if the calendar has event or not and add it if the slot is empty. If not return failure.
			if (addEventToCalendar(
						calendarSTime,
						calendarETime,
						$scope.calendarEventType.selectedOption.name,
						isHoursDone) == true) {
				//if the entry is work hours type post it as such into the database
				if ($scope.calendarEventType.selectedOption.id == 1) {

					var l_hoursToSave = hourscalculation();
					var l_updatedflex = flexhour();
					var postinghours = new HourFactory();

					//Save these values to database
					postinghours.id = userID;
					postinghours.start = convertTimesToDateFormat(
							g_selectedDate,
							$scope.startTime);
					postinghours.end = convertTimesToDateFormat(
							g_selectedDate,
							$scope.endTime);
					postinghours.title = $scope.calendarEventType.selectedOption.name;
					postinghours.worktimeday = l_hoursToSave;
					postinghours.sdate = g_selectedDate;
					postinghours.itemtype = $scope.calendarEventType.selectedOption.id;
					postinghours.flex = l_updatedflex;
					postinghours.week = getWeekNumber();
					postinghours.$save();
					$scope.g_selectedDate = g_selectedDate;
					//console.log("updated flex", l_updatedflex);
					$scope.flex = l_updatedflex;
					UserFactory.update({
						"id": userID
					}, {
						"flex": l_updatedflex
					}, function () {
						console.log("updated succesfully");
					});
					
				} else {
					//The entry is other than work hours so it's posted to the database differently.
					var postinghours = new HourFactory();
					postinghours.sdate = g_selectedDate;
					postinghours.start = g_selectedDate; //if using calendarStime it bugs and moves the event to previous day?
					postinghours.end = calendarETime;
					postinghours.id = userID;
					postinghours.itemtype = $scope.calendarEventType.selectedOption.id;
					postinghours.title = $scope.calendarEventType.selectedOption.name;
					postinghours.allDay = true;
					postinghours.week = getWeekNumber();
					postinghours.$save();
				}
			} else { 
				//Event overlaps with another event.
				//Do stuff
				swal("Event overlaps!");
			}
		}
	}
});
