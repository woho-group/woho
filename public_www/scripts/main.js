var app = angular.module("WoHoApp", ["ngRoute", "Controllers", "Calendarctrl"]);

// Controllers to be aliased to xxxxCtrl
// eg. AddUserController -> AddUserCtrl
app.config(['$routeProvider', '$httpProvider',
		function ($routeProvider, $httpProvider) {
			$routeProvider.
			when('/overview', {
				templateUrl: 'views/view-User-Overview.html',
				//,controller: 'OverviewController'
			}).when('/vacations', {
				templateUrl: 'views/view-Vacation-Days.html',
				controller: 'VacationsController'
			}).when('/flexhours', {
				templateUrl: 'views/view-Flexhours.html'
				//,controller: 'FlexHoursController'
			}).when('/vacations-all', {
				templateUrl: 'views/view-Vacations-All.html'
				//,controller: 'FlexHoursController'
			}).when('/myprofile', {
				templateUrl: 'views/view-My-Profile.html',
				controller: 'MyProfileController'
			}).when('/listandsearch', {
				templateUrl: 'views/view-ListandSearchUser.html',
				controller: 'ListandSearchUserController'
			}).when('/addemployee', {
				templateUrl: 'views/view-Add-User.html',
				controller: 'AddUserController'
			}).when('/manageusers', {
				templateUrl: 'views/view-Manage-Users.html'
			}).when('/login', {
				templateUrl: 'views/login.html',
				controller: 'LoginController'
			}).when('/calendar', {
				templateUrl: 'views/calendar.html',
				controller: 'CalendarController'
			}).when('/index', {
				templateUrl: '/index.html',
				controller: 'IndexController'
			}).otherwise({
				redirectTo: '/login'
			});

			/*
			Interceptor adds token to every http request if token exists in localStorage.
			Token validity will be checked in app.js.
			 */
			$httpProvider.interceptors.push(['$q', '$location', 'TokenFactory', function ($q, $location, TokenFactory) {
						return {
							'request': function (config) {
								config.headers = config.headers || {};
								config.headers.token = TokenFactory.getToken();
								return config;
							},
							'responseError': function (response) {
								if (response.status === 401 || response.status === 403) {
									$location.path('/login');
								}
								return $q.reject(response);
							}

						};
						}
				]);

		}
	]);

// This directive Adds functionality to index.html tag <navigation-bar></navigation-bar>
// When <navigation-bar> in index.html is called it will write the navigationbar.html view into the page
// Notice  the - in <navigation-bar> transforms the letter b into upper case B which
// is used in the app.directive as navigationBar
// navigation-bar -> navigationBar
app.directive('navigationBar', function () {
	return {
		restrict: 'E',
		templateUrl: 'views/view-Navigationbar.html'
	};
});

app.directive('listandSearchUser', function () {
	return {
		restrict: 'E',

		templateUrl: 'views/view-ListandSearchUser.html'
	};
});
app.directive('addUser', function () {
	return {
		restrict: 'E',

		templateUrl: 'views/view-Add-User.html'
	};
});

app.directive('viewVacations', function () {
	return {
		restrict: 'E',
		templateUrl: 'views/view-Vacation-Days.html'
	};
});

app.directive('viewFlexhours', function () {
	return {
		restrict: 'E',
		templateUrl: 'views/view-Flexhours.html'
	};
});
