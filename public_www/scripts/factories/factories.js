var app = angular.module("Factories", ["ngResource"]);

app.factory("UserFactory", function ($resource) {

	return $resource("/api/employees/:id", {
		"id": "@id"
	}, {
		update: {
			method: 'PUT'
		}
	});
});

app.factory("HourFactory", function ($resource) {
	return $resource("/api/hoursdaily/:id", {
		"id": "@id"
	});
});

app.factory("CheckLoginFactory", function ($resource) {
	return $resource("/api/checklogin/:token", {
		"token": "@token"
	});
});

app.factory("CheckPwordFactory", function ($http) {
	check = function (id, pword) {
		var connection = $http({
				method: "GET",
				url: "/api/checkpword/",
				params: {
					"id": id,
					"pword": pword
				},
				headers: {
					"Content-type": "application/json"
				}

			}).error(function () {
				console.log("Error in checkpword factory!");
			});
		return connection;
	};

	return {
		check: check
	};
	
});

app.factory("WeekFactory", function ($resource) {
	return $resource("/api/weeks/", {
		
	},
	{
	query:{ 
		url: '/api/weeks/:id/:week/:year', 
		method: 'GET',
		isArray: true,
		params:{ 
			id:'@id', 
			week: '@week',
			year: '@year'
		}}
	});
});

app.factory("TokenFactory", function () {

	var factory = {};
	var Authtoken = {};
	var userId = false;
	var objectId = false;

	factory.setToken = function (token, userid, objectid, accounttype) {
		localStorage.token = token;
		localStorage.userid = userid;
		localStorage.objectid = objectid;
		localStorage.accounttype = accounttype;
		userId = userid;
		objectId = objectid;
		Authtoken = token;
		console.log("Storage token set to: " + localStorage.token);
		console.log("Storage user id set to: " + localStorage.userid);
		console.log("Storage object id set to: " + localStorage.objectid);
	};

	factory.getToken = function () {
		if (localStorage.token && localStorage.token.length > 16) {
			return localStorage.token;
		} else {
			return false;
		}
		//return Authtoken;
	};

	factory.setId = function (id) {
		localStorage.userid = id;
		userId = id;
	};

	factory.unsetAll = function () {
		localStorage.token = false;
		localStorage.userid = false;
		localStorage.objectid = false;
	};

	factory.getId = function () {
		if (localStorage.userid) {
			return localStorage.userid;
		} else {
			return false;
		}
	};
	
	factory.setObjectId = function (objectid) {
		localStorage.objectid = objectid;
		objectId = objectid;
	};
	
	factory.getObjectId = function () {
		if (localStorage.objectid) {
			return localStorage.objectid;
		} else {
			return false;
		}
	};
	
	factory.getAccounttype = function () {
		if (localStorage.accounttype) {
			return localStorage.accounttype;
		} else {
			return 0;
		}
	};

	return factory;

});

app.factory("LoginFactory", function ($http) {
	//Sends login information to server, returns whatever server returns.
	//Password is sent in plain text, ideally you want to use https for security etc.
	login = function (email, pword) {
		var connection = $http({
				method: "POST",
				url: "login",
				data: {
					"email": email,
					"pword": pword
				},
				headers: {
					"Content-type": "application/json"
				}

			}).error(function () {
				console.log("Error in login factory!");
			});
		return connection;
	};

	return {
		login: login
	};

});
