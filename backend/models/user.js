var mongoose = require("mongoose");

var schema = mongoose.Schema;

module.exports = mongoose.model("userdetail", new schema({

			id: Number, // changed name workerid -> userid
			firstname: String,
			lastname: String,

			
			worktimetotal: String, //contractual work hours per day example: 7:30 hours/day

			workphone: String,
			homephone: String,
			
			workStartTime: String,
			workEndTime: String,

			datestartedworking: {
				type: Date,
			default:
				Date.now
			}, //the date user entered the company defaults to todays date if left null
			email: {
				type: String,
				required: true,
				unique : true
			},
			accounttype: {
				type: Number,
				min: 1,
				max: 2,
			default:
				1
			}, //1 = normal , 2 = admin
			password: {
				type: String
			},
			salt: {
				type: String
			},
			flex: {
				type: String,
                default: "00:00"
			}
		}));
