var mongoose = require("mongoose");

var Schema = mongoose.Schema;

module.exports = mongoose.model("HoursDone", new Schema({
			userid: String, //was the original ID now userid and String
			itemtype: {
				type: Number,
				min: 1,
				max: 4,
			default:
				1
			}, //1=Hours done, 2= Sick Leave, 3= annual vacation, 4= compensated leave
			title: String,
			sdate: Date,
			name: String,
			worktimeday: String,
			selecteddate: Date, //date is this needed? might be unneccessary since starttime + endtime are both dates
			//in minutes can be empty is for storing total duration of the workhours eg. endtime - starttime = total worktime
			start: Date,
			end: Date,
			editable: {
				type: Boolean,
			default:
				false
			},
			allDay: Boolean,
			flex: String,
			week: String
			//end of workperiod
		}));
