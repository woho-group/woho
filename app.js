var express = require("express");
var app = express();
var path = require("path");
var bodyParser = require("body-parser");
var mongoose = require("mongoose"); 
var apiRouter = express.Router();
var path = require("path");
var UserModel = require("./backend/models/user");
var HoursDone = require("./backend/models/hoursdone");
var config = require("./config");
var jwt = require("jsonwebtoken");
var crypto = require("crypto");
//var moment = require("moment");
var moment = require('moment-timezone'); //moment with timezones if needed

mongoose.connect(config.database);
app.set("tokenSecret", config.secret);
app.use(express.static(path.join(__dirname, "public_www")));
app.use(bodyParser.json());


apiRouter.use(function (req, res, next) {

	//Checking if token exists and is correct
	//If not, returns an error and user can't access the API

 	var token = req.headers.token;

 	if (token) {
 		jwt.verify(token, app.get("tokenSecret"), function (err, decoded) {
 			if (err) {
 				//Token was wrong
 				console.log("Wrong token");
 				res.status(403).send(JSON.stringify({
 						success: false,
 						message: "Wrong token"
 					}));
 			} else {
 				//Token is ok, moving to next handler.
				//Also attaching decoded id and decoded accountype to request so they can be used if needed.
 				console.log("Token ok!");
				req.checkedid = decoded.id;
				req.accounttype = decoded.accounttype;
				req.objectid = decoded.objectid;
 				next();
 			}
 		});
 	} else {
 		//Token missing
 		console.log("No token");
 		res.status(403).send(JSON.stringify({
 				success: false,
 				message: "No token"
 			}));
 	}
 });


//get list of employee
apiRouter.get("/employees", function (req, res) {
	//Only accounttype 2 (=admin) can request list of users.
	if (req.accounttype == 2) {
	UserModel.find(function (err, user, count) {
		if (err) {
			console.log("data not found");
		} else {
			res.send(JSON.stringify(user));
		}
	});
	} else {
		res.status(403).send(JSON.stringify({
 				success: false,
 				message: "No access"
 			}));
	}
});

//post one employee
apiRouter.post("/employees/:id", function (req, res) {
	//Only admin users are allowed to "post" employees
	if (req.accounttype == 2) {
		var saltforuser = crypto.randomBytes(32).toString("base64");
		var encryptedpassword = encryptpassword(req.body.password, saltforuser);
		var userdetail = new UserModel({
				id: req.body.id,
				firstname: req.body.firstname,
				lastname: req.body.lastname,
				workStartTime: req.body.workStartTime,
				workEndTime: req.body.workEndTime,
				workphone: req.body.workphone,
				homephone: req.body.homephone,
				worktimetotal: req.body.worktimetotal,
				email: req.body.email,
				accounttype: req.body.accounttype,
				flex: req.body.flex,
				password:encryptedpassword,
				salt:saltforuser
			});
		userdetail.save(function (err, userdetail, count) {
			if (err) {
				console.log("cannot save data");
			} else {
				res.send("success");
			}
		});
	} else {
		res.status(403).send(JSON.stringify({
 				success: false,
 				message: "No access"
 			}));
	}
});

// get one employee with Id
apiRouter.get("/employees/:id", function (req, res) {
	//console.log("get id" + req.params.id);
	console.log("User " + req.checkedid + " is requesting data for user " + req.params.id + ", with account level " + req.accounttype);
	//Check if user has access to request. If requested id is the same as user id, proceed.
	//Alternatively if user accounttype is 2 (=admin), also proceed. Otherwise no access.
	if (req.checkedid == req.params.id || req.accounttype == 2) {
		UserModel.findOne({
			"id": req.params.id
		}, function (err, user) {
			if (err) {
				console.log("cannot find user");
				console.log(err);
				var empty = {};
				res.send(JSON.stringify(empty));
			} else {
				res.send(JSON.stringify(user));
				console.log(user);
			}
		});
	} else {
		res.status(403).send(JSON.stringify({
 				success: false,
 				message: "No access"
 			}));
	}
});

//Update one employee
apiRouter.put("/employees/:id", function (req, res) {
	//Check if admin or user is editing own details, otherwise no access
	if (req.checkedid == req.params.id || req.accounttype == 2) {
		console.log(req.params.id);
		console.log(req.body);
		
		//Find the user from db first
		UserModel.findOne({
			"id": req.params.id
		}, function (err, user) {
			if (err) {
				console.log("Cannot find user to update (put)!");
				console.log(err);
				var empty = {};
				res.status(404).send(JSON.stringify({
 				success: false,
 				message: "No user data found."
				}));
			} 
			
			//User found from db
			else {
				console.log("What we found from db:");
				console.log(user.password);
				console.log("What we are getting from req");
				console.log(req.body.password);
				
				//If incoming request has password and it matches with the one in db, do this
				if (req.body.password) {
					if (user.password == req.body.password) {
						console.log("updating everything else!");
						UserModel.update({
							"id": req.params.id
						}, req.body, function (err, item) {
							if (err) {
								console.log("error in updating data");
							}
							res.send("successfully updated");
						});
					} 
					//Incoming request had password but didn't match one in db, updating password only
					else {
							console.log("Updating password only!");
							var saltforuser = crypto.randomBytes(32).toString("base64");
							var encryptedpassword = encryptpassword(req.body.password, saltforuser);
							user.password = encryptedpassword;
							user.salt = saltforuser;
							user.save(function (err) {
								if (err) {
									console.log("Error updating password!");
								}
								res.send("successfully updated");
							});
					}
				} else {
					UserModel.update({
							"id": req.params.id
						}, req.body, function (err, item) {
							if (err) {
								console.log("error in updating data");
							}
							res.send("successfully updated");
						});
				}
			}
		});
	
	} else {
		res.status(403).send(JSON.stringify({
 				success: false,
 				message: "No access"
 			}));
	}
});

//delete one employee
apiRouter.delete ("/employees/:id", function (req, res) {
	//Only admin can delete users
	if (req.accounttype == 2) {
		UserModel.remove({
			"id": req.params.id
		}, function (err, item) {
			if (err) {
				console.log("cant find data");
			}
			res.send("success");
		});
	} else {
		res.status(403).send(JSON.stringify({
 				success: false,
 				message: "No access"
 			}));
	}
});

//UPdating User Flex hour

apiRouter.put("employees/:id", function (req, res) {
	//Only user can edit/post own hours. Alternatively admin has access.
	if (req.checkedid == req.params.id || req.accounttype == 2) {
		console.log("id in emplyee api", req.body.id);
		console.log("flex", req.body.g_flexHour);
		UserModel.update({
			"id": req.body.id
		}, {
			"flex": req.body.g_flexHour
		}, function (err, item) {
			if (err) {
				console.log("error in updating");
			}
			res.send("successfully updated");
		});
	} else {
		res.status(403).send(JSON.stringify({
 				success: false,
 				message: "No access"
 			}));
	}
});

//delete calendar entry for id

apiRouter.delete ("/hoursdaily/:id", function (req, res) {
	//Anyone can delete their hours
	if (req.accounttype == 1 || req.accounttype == 2) {
		HoursDone.remove({
			"_id": req.params.id
		}, function (err, item) {
			if (err) {
				console.log("cant find data");
			}
			res.send("success");
		});
	} else {
		res.status(403).send(JSON.stringify({
 				success: false,
 				message: "No access"
 			}));
	}
});

//get daily hours to print on calendar
apiRouter.get("/hoursdaily/:id", function (req, res) {
	if (req.checkedid == req.params.id || req.accounttype == 2) {
		console.log(req.params.id);
		HoursDone.find({
			"userid": req.params.id
		}, function (err, item) {

			if (err) {
				var empty = {};
				res.send(JSON.stringify(empty));

			} else {
				res.send(JSON.stringify(item));
				console.log("getting value from database hour done");
				console.log(item);
			}
		});
	} else {
		res.status(403).send(JSON.stringify({
 				success: false,
 				message: "No access"
 			}));
	}

});

//get weekly data
apiRouter.get("/weeks/:id/:week/:year", function (req, res) {
	console.log("we are in weeks!");
	console.log("reqid " + req.params.id);
	console.log("reqweek " + req.params.week);
	console.log("reqyear " + req.params.year);
	if (req.checkedid == req.params.id || req.accounttype == 2) {
		HoursDone.find({
			"userid": req.params.id,
			"week": req.params.week
		}, function (err, item) {

			if (err) {
				var empty = {};
				res.send(JSON.stringify(empty));

			} else {
				//filter the results so that only data from selected year is sent
				result = filterYear(item, req.params.year);
				console.log("getting week data from db");
				res.send(JSON.stringify(result));
				//console.log(result);
			}
		});
	} else {
		res.status(403).send(JSON.stringify({
 				success: false,
 				message: "No access"
 			}));
	}
});

//Check password serverside
apiRouter.get("/checkpword/", function (req, res) {
	console.log("Entered checkpword");
	console.log(req.checkedid + " ja " + req.query.id + " ja " + req.accounttype);
	if (req.checkedid == req.query.id || req.accounttype == 2) {
		console.log("Entered checkpword actual");
			UserModel.findOne({
			"id": req.query.id
			}, function (err, user) {
			if (err) {
				console.log("cannot find user");
				console.log(err);
				res.status(404).send(JSON.stringify({
					success: false,
					message: "No user data found."
				}));
			} else {
				console.log("Match passwords..");
				var encryptedpassword = encryptpassword(req.query.pword, user.salt);
				if (user.password != encryptedpassword) {
					console.log("Passwords don't match!");
					res.send(JSON.stringify({
							success: false,
							message: "NoMatch"
						}));
				} else {
					console.log("Passwords match!");
					res.send(JSON.stringify({
							success: true,
							message: "PasswordMatch"
						}));
				}
			}
		});
	} else {
		res.status(403).send(JSON.stringify({
 				success: false,
 				message: "No access"
 		}));
			
	}
	
});

function filterYear(arr, value) {
  //loop through our array backwards, delete entries that don't match the year we are looking for
  for (var i = arr.length -1; i >= 0; i--) { 
	//console.log(arr[i]['sdate']);
	if(arr[i]['sdate'].getFullYear() == value) {
      
	} else {
	 //Remove from array if year didn't match
      arr.splice(i, 1);
    }
  };
  return arr;
}

//post daily hours
apiRouter.post("/hoursdaily/:id", function (req, res) {
	if (req.checkedid == req.params.id || req.accounttype == 2) {
		console.log("saving users hours in database");
		console.log(req.body.worktimeday);
		console.log("flex in hourdaily api", req.body.flex);
		var dailyhour = new HoursDone({
				userid: req.body.id,
				title: req.body.title, //title for calendar event
				start: req.body.start, //event start time in dateformat
				end: req.body.end, //event end time in dateformat
				itemtype: req.body.itemtype,
				sdate: req.body.sdate,
				worktimeday: req.body.worktimeday,
				flex: req.body.flex,
				week: req.body.week,
				allDay: req.body.allDay
				
			});
		console.log("s & e: "+req.body.start+req.body.end);
		console.log(dailyhour);
		dailyhour.save(function (err, userhour, count) {
			if (err) {
				console.log("cannot save data");
			} else
				res.send("success");
		});
	} else {
		res.status(403).send(JSON.stringify({
 				success: false,
 				message: "No access"
 			}));
	}
});

apiRouter.get("/login", function (req, res) {
	console.log("Where do we go from here?");
	//res.redirect("/index");
});

//login attempt
app.post("/login", function (req, res) {

	//Check if email exists in database
	UserModel.findOne({
		email: req.body.email
	}, function (err, user) {
		console.log("Login attempt");
		if (err)
			throw err;
		//if user doesn't exist in mongo
		if (!user) {
			res.send(JSON.stringify({
					success: false,
					message: "NoUser"
				}));
		} else if (user) {
			//user exists, encrypt the password so it can be compared to the one in mongo
		var encryptedpassword = encryptpassword(req.body.pword, user.salt);
			console.log("Password in app from controller: " + req.body.pword);
			console.log("Password from mongo: " + user.password);
			console.log("Salt from mongo: " + user.salt);
			console.log("Password after encryption: " + encryptedpassword);
			//password doesn't match with the password in mongo
			if (user.password != encryptedpassword) {
				res.send(JSON.stringify({
						success: false,
						message: "WrongPassword"
					}));
			} else {
				//Email exists and password is correct, create token
				//Token includes id and accountype which can be later extracted if needed
				console.log("Creating token...");
				var token = jwt.sign({
						id: user.id,
						objectid:user._id,
						accounttype: user.accounttype
					}, app.get("tokenSecret"), {
						expiresIn: 36000000
					});
				//Send token to client, it will be attached to every http request from now on
				res.send(JSON.stringify({
						success: true,
						message: "TokenGranted",
						token: token,
						userid: user.id,
						objectid: user._id,
						accounttype: user.accounttype
					}));
			}
		}
	});
});

apiRouter.get("/checklogin/:token", function (req, res) {
	console.log("check login api called!");

	res.send("success");
});

//logging out
apiRouter.post("/logout", function (req, res) {});

//encrypts password and returns the encrypted version so we can compare it to the one saved in database
function encryptpassword(password, salt) {

	//Checks if salt exists, if it doesn't, returns a zero to ensure login fails
	if (salt) {
		var cipher = crypto.createCipher("aes192", salt);
		var encrypted = cipher.update(password, "utf8", "hex");
		encrypted += cipher.final("hex");
		console.log(password + " encrypted is:");
		console.log(encrypted);
		return encrypted;
	}
	return 0;
}

//Do we ever need to decrypt the password? Here's a function to do so if needed
function decryptpassword(password, salt) {
	// if (salt) {
	// 	var decipher = crypto.createDecipher("aes192", salt);
	// 	var decrypted = decipher.update(password, "hex", "utf8");
	// 	decrypted += decipher.final("utf8");
	// 	console.log(decrypted);
	// 	return decrypted;
	// }
	// return 0;
}

app.use("/api", apiRouter);
app.listen(3000);
console.log("Started server on port 3000");
